# AHK Scripts

Placed into the scripts folder. 
Feel free to use these.
Likely not updated often, so they can become out of date.

## Current Scripts

1. ![PushToTalk.ahk](https://gitlab.com/rkett/autohotkey-scripts/blob/master/Scripts/PushToTalk.ahk) -- Script that allows you to bind a key for microphone push to talk cababilities. Displays status in tooltip on top right of screen.
              


