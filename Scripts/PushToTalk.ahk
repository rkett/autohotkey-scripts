;*****************************************************************
;   Author: Ross Kett
;   Affiliation: None
;   Date: 3/3/2018
;   
;   PushToTalk -- Script that allows you to bind a key for 
;                 microphone push to talk cababilities. Displays
;                 status in tooltip on top right of screen.
;*****************************************************************

#Persistent ;Always running, until closed.
#SingleInstance force ;One script allowed open of this type.

micNumber = 17 ;Location of mic -- not universal See: https://autohotkey.com/docs/commands/SoundSet.htm#Ex
SysGet, monintorPosX, 78 ;A_ScreenWidth
SysGet, monintorPosY, 79 ;A_ScreenHeight

/*
* Toggles microphone on/off (variable dependant.)
* micNumber [Int] - Mixer value of microphone device.
* monitorPosX [Int] - Max. value of monitor's X position.
* monitorProY [Int] - Max value of monitor's Y position.
* toggleState [Int] - On/off/toggle (1/0/-1).
*/
muteToggle(micNumber,monintorPosX,monintorPosY,toggleState) {
    SoundGet, micStatus, MASTER, MUTE, micNumber
    SoundSet, toggleState, MASTER, MUTE, micNumber
    ToolTip, Microphone:%micStatus%, %monintorPosX%, %monintorPosY%
    return
}

/*
* Push to talk -- Bound to "mouse 4" or "XButton2".
*/
$XButton2::
	muteToggle(micNumber,monintorPosX,monintorPosY,0) ;Default state.
	KeyWait, Xbutton2
	muteToggle(micNumber,monintorPosX,monintorPosY,1) ;Button is in "down" state.
	return
	